#!/bin/bash

GAME_PREFIX="smb_1"

BEG_LEN=10
ADV_LEN=30
EXP_LEN=50
BEG_X_LEN=3
ADV_X_LEN=5
EXP_X_LEN=10
MAS_LEN=10
MAS_X_LEN=10
STORY_LEN=100

CTRL_DPAD_LEFT=f
CTRL_DPAD_RIGHT=h
CTRL_DPAD_DOWN=g
CTRL_R=w
CTRL_UNLIMIT_SPEED=Tab

function main {
    for i in 5 4 3 2 1; do
        echo "Beginning in $i"
        sleep 1
    done

    xdotool keydown $CTRL_UNLIMIT_SPEED

    ss-difficulty $BEG_LEN "$GAME_PREFIX/beginner"
    next-diff
    ss-difficulty $ADV_LEN "$GAME_PREFIX/advanced"
    next-diff
    ss-difficulty $EXP_LEN "$GAME_PREFIX/expert"
    next-diff
    ss-difficulty $BEG_X_LEN "$GAME_PREFIX/beginner_extra"
    next-diff
    ss-difficulty $ADV_X_LEN "$GAME_PREFIX/advanced_extra"
    next-diff
    ss-difficulty $EXP_X_LEN "$GAME_PREFIX/expert_extra"
    next-diff
    ss-difficulty $MAS_LEN "$GAME_PREFIX/master"
    # next-diff
    # ss-difficulty $MAS_X_LEN "$GAME_PREFIX/master_extra"
    # next-diff
    # ss-difficulty $STORY_LEN "$GAME_PREFIX/story"

    xdotool keyup $CTRL_UNLIMIT_SPEED
}

#Args:
#1: Difficulty length
#2: Directory
function ss-difficulty {
    mkdir -p $2
    for ((i = 1; i <= $1; i++)); do
        echo "Screenshotting $2/$i"
        ss-and-next "$2/$i"
    done
}

function ss-and-next {
    maim -u > $1.png
    xdotool keydown $CTRL_DPAD_RIGHT
    sleep 0.05
    xdotool keyup $CTRL_DPAD_RIGHT
    sleep 1 #Give the game time to load
}

function next-diff {
    xdotool keydown $CTRL_DPAD_DOWN
    sleep 0.05
    xdotool keyup $CTRL_DPAD_DOWN
    xdotool keydown $CTRL_DPAD_LEFT
    xdotool keydown $CTRL_R
    sleep 3
    xdotool keyup $CTRL_R
    xdotool keyup $CTRL_DPAD_LEFT
}

main
